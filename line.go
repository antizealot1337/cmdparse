package cmdparse

import "unicode"

const (
	// DefaultQuotes are the normal quote characters for a command line
	DefaultQuotes = "\"'`"
)

// Line is similar to LineWithQuotes except it uses DefaultQuotes for the
// quote characters. Like LineWithQuotes, Runes, and Bytes it will return as
// error if a quote is encountered but no match is found later.
func Line(line string) (args []string, err error) {
	return LineWithQuotes(line, DefaultQuotes)
} //func

// LineWithQuotes parses a line line a comand line parser would. It accepts a
// string of characters that act as quotes used to provided arguments that
// contain spaces and words.
func LineWithQuotes(line, quotes string) (args []string, err error) {
	if asciiOnly(line) {
		args, err = Bytes([]byte(line), []byte(quotes))
	} else {
		args, err = Runes([]rune(line), []rune(quotes))
	} //if

	return
} //func

// asciiOnly check if a string contains only ascii character.
func asciiOnly(s string) bool {
	for _, c := range []rune(s) {
		if c > unicode.MaxASCII {
			return false
		} //if
	} //for
	return true
} //func
