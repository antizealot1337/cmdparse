package cmdparse

import "testing"

func TestRuneBuffAdd(t *testing.T) {
	var rb runebuff
	var b = '1'

	rb.add(b)

	if expected, actual := b, rb[0]; actual != expected {
		t.Errorf("Expected to add %d but didn't")
	} //if
} //func

func TestRuneBuffReset(t *testing.T) {
	rb := runebuff{'0', '1', '2'}
	rb.reset()

	if expected, actual := 0, len(rb); actual != expected {
		t.Errorf("Expected len to be %d but was %d after reset", expected, actual)
	} //if
} //func

func TestRuneIn(t *testing.T) {
	// The values to check against
	s := []rune{'a', 'b', 'c'}

	// Make sure 'x' isn't in the slice
	if b := 'x'; runeIn(b, s) {
		t.Errorf("Did not expecte %b to be in %v", b, s)
	} //if

	// Make sure 'y' isn't in the slice
	if b := 'y'; runeIn(b, s) {
		t.Errorf("Did not expecte %b to be in %v", b, s)
	} //if

	// Make sure 'z' isn't in the slice
	if b := 'z'; runeIn(b, s) {
		t.Errorf("Did not expecte %b to be in %v", b, s)
	} //if

	// Make sure 'a' is in the slice
	if b := 'a'; !runeIn(b, s) {
		t.Errorf("Expected %b to be in %v", b, s)
	} //if

	// Make sure 'b' is in the slice
	if b := 'b'; !runeIn(b, s) {
		t.Errorf("Expected %b to be in %v", b, s)
	} //if

	// Make sure 'c' is in the slice
	if b := 'c'; !runeIn(b, s) {
		t.Errorf("Expected %b to be in %v", b, s)
	} //if
} //func

func TestRunes(t *testing.T) {
	checkarg := func(args []string, idx int, exarg string) {
		if actual := args[idx]; actual != exarg {
			t.Errorf(`Expected args at %d to be "%s" but was "%s"`,
				idx, exarg, actual)
		} //if
	} //func

	// Set the line and quotes
	line, quotes := []rune("abc"), []rune(`"`)

	// Parse the line
	args, err := Runes(line, quotes)

	// Check for an error
	if err != nil {
		t.Error("Unexpected error", err)
	} //if

	// Check the len of the args
	if expected, actual := 1, len(args); actual != expected {
		t.Errorf("Expected length of %d but was %d", actual, expected)
	} else {
		// Check the args
		checkarg(args, 0, "abc")
	} //if

	// Set the line
	line = []rune("a b c")

	// Parse the line
	args, err = Runes(line, quotes)

	// Check for an error
	if err != nil {
		t.Error("Unexpected error", err)
	} //if

	// Check the len of the args
	if expected, actual := 3, len(args); actual != expected {
		t.Errorf("Expected length of %d but was %d", expected, actual)
	} else {
		// Check the args
		checkarg(args, 0, "a")
		checkarg(args, 1, "b")
		checkarg(args, 2, "c")
	} //if

	// Set the line
	line = []rune(`a "b c"`)

	// Parse the line
	args, err = Runes(line, quotes)

	// Check for an error
	if err != nil {
		t.Error("Unexpected error", err)
	} //if

	// Check the len of the args
	if expected, actual := 2, len(args); actual != expected {
		t.Errorf("Expected length of %d but was %d", expected, actual)
	} else {
		// Check the args
		checkarg(args, 0, "a")
		checkarg(args, 1, "b c")
	} //if

	// Set the line
	line = []rune(`a "b c`)

	// Parse the line
	_, err = Runes(line, quotes)

	// Check for an error
	if err != ErrNoQuoteTerm {
		t.Error("Expected error for missing quote")
	} //if
} //func

func BenchmarkRuneBuffNoCap(b *testing.B) {
	var buff runebuff
	var c = '😊'

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buff.add(c)
	} //for
} //func

func BenchmarkRuneBuffCap(b *testing.B) {
	buff := make(runebuff, 0, b.N)
	var c = '😊'

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buff.add(c)
	} //for
} //func
