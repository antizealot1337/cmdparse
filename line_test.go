package cmdparse

import "testing"

func TestLine(t *testing.T) {
	same := func(line string) {
		args1, err := Line(line)

		if err != nil {
			t.Fatal("Unexpected error", err)
		} //if

		args2, err := LineWithQuotes(line, DefaultQuotes)

		if err != nil {
			t.Fatal("Unexpected error", err)
		} //if

		for i := range args1 {
			if expected, actual := args1[i], args2[i]; actual != expected {
				t.Errorf(`Expected arg1("%s") and arg2("%s") at %d to be the same`,
					expected, actual, i)
			} //if
		} //for
	} //func

	same("abc")
	same(`a b c`)
	same(`a "b c"`)
	same(`a "b c" ☺`)
} //func

func TestLineWithQuotes(t *testing.T) {
	same := func(line string) {
		args1, err := LineWithQuotes(line, DefaultQuotes)

		if err != nil {
			t.Fatal("Unexpected error", err)
		} //if

		var args2 []string
		if asciiOnly(line) {
			args2, err = Bytes([]byte(line), []byte(DefaultQuotes))
		} else {
			args2, err = Runes([]rune(line), []rune(DefaultQuotes))
		} //if

		if err != nil {
			t.Fatal("Unexpected error", err)
		} //if

		for i := range args1 {
			if expected, actual := args1[i], args2[i]; actual != expected {
				t.Errorf(`Expected arg1("%s") and arg2("%s") at %d to be the same`,
					expected, actual, i)
			} //if
		} //for
	} //func

	same(`a "b c" d`)
	same(`a "b c" ☺`)
} //func

func TestAsciiOnly(t *testing.T) {
	if line := "abcd"; !asciiOnly(line) {
		t.Errorf(`Expected line "%s" to be ascii only`, line)
	} //if

	if line := "✔"; asciiOnly(line) {
		t.Errorf(`Expected line "%s" to be unicode`, line)
	} //if
} //func

func BenchmarkStraightRunes(b *testing.B) {
	line := []rune("abcdefghijklmnopqrstuvwxyz")
	quotes := []rune(DefaultQuotes)
	for i := 0; i < b.N; i++ {
		//Runes([]rune(line), []rune(quotes))
		Runes(line, quotes)
	} //for
} //func

func BenchmarkLineWithQuotes(b *testing.B) {
	line := "abcdefghijklmnopqrstuvwxyz"

	for i := 0; i < b.N; i++ {
		LineWithQuotes(line, DefaultQuotes)
	} //for
} //func

func BenchmarkLine(b *testing.B) {
	line := "abcdefghijklmnopqrstuvwxyz"
	for i := 0; i < b.N; i++ {
		Line(line)
	} //for
} //func
