package cmdparse

var (
	byteSpaces = []byte(" \t\n\r")
)

type bytebuff []byte

func (bb *bytebuff) add(b byte) {
	*bb = append(*bb, b)
} //func

func (bb *bytebuff) reset() {
	*bb = (*bb)[:0]
} //func

func byteIn(b byte, s []byte) bool {
	for _, sb := range s {
		if sb == b {
			return true
		} //if
	} //for
	return false
} //func

// Bytes parses a line from bytes into a slice of strings. If a character from
// quotes is encountered then it will add add all into a string until the
// matching character is found. If not matching terminating quote is found an
// error is returned.
func Bytes(line, quotes []byte) (s []string, err error) {
	var (
		buff      = make(bytebuff, 0, len(line))
		lastquote byte
	)

	checkbuff := func() {
		// Check if the buffer has any contents
		if len(buff) > 0 {
			// Add the string
			s = append(s, string(buff))

			// Reset the buffer
			buff.reset()
		} //if
	} //func

	// Loop through the bytes in the line
	for _, b := range line {
		// Determine what to do with the byte
		switch {
		case lastquote != 0 && b != lastquote:
			buff.add(b)
		case lastquote != 0 && b == lastquote:
			lastquote = 0
		case byteIn(b, quotes):
			checkbuff()
			lastquote = b
		case byteIn(b, byteSpaces):
			checkbuff()
			continue
		default:
			buff.add(b)
		} //switch
	} //for

	// Final check of the buffer
	checkbuff()

	// Check for a missing termination quote
	if lastquote != 0 {
		err = ErrNoQuoteTerm
	} //if

	return
} //func
