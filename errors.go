package cmdparse

import "errors"

var (
	// ErrNoQuoteTerm is returned when no terminating quote is found in a line.
	ErrNoQuoteTerm = errors.New("no matching terminating quote found")
)
