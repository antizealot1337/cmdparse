package cmdparse

import (
	"testing"
)

func TestByteBuffAdd(t *testing.T) {
	var bb bytebuff
	var b byte = '1'

	bb.add(b)

	if expected, actual := b, bb[0]; actual != expected {
		t.Errorf("Expected to add %d but didn't")
	} //if
} //func

func TestByteBuffReset(t *testing.T) {
	bb := bytebuff{0, 1, 2}
	bb.reset()

	if expected, actual := 0, len(bb); actual != expected {
		t.Errorf("Expected len to be %d but was %d after reset", expected, actual)
	} //if
} //func

func TestByteIn(t *testing.T) {
	// The values to check against
	s := []byte{'a', 'b', 'c'}

	// Make sure 'x' isn't in the slice
	if b := byte('x'); byteIn(b, s) {
		t.Errorf("Did not expecte %b to be in %v", b, s)
	} //if

	// Make sure 'y' isn't in the slice
	if b := byte('y'); byteIn(b, s) {
		t.Errorf("Did not expecte %b to be in %v", b, s)
	} //if

	// Make sure 'z' isn't in the slice
	if b := byte('z'); byteIn(b, s) {
		t.Errorf("Did not expecte %b to be in %v", b, s)
	} //if

	// Make sure 'a' is in the slice
	if b := byte('a'); !byteIn(b, s) {
		t.Errorf("Expected %b to be in %v", b, s)
	} //if

	// Make sure 'b' is in the slice
	if b := byte('b'); !byteIn(b, s) {
		t.Errorf("Expected %b to be in %v", b, s)
	} //if

	// Make sure 'c' is in the slice
	if b := byte('c'); !byteIn(b, s) {
		t.Errorf("Expected %b to be in %v", b, s)
	} //if
} //func

func TestBytes(t *testing.T) {
	checkarg := func(args []string, idx int, exarg string) {
		if actual := args[idx]; actual != exarg {
			t.Errorf(`Expected args at %d to be "%s" but was "%s"`,
				idx, exarg, actual)
		} //if
	} //func

	// Set the line and quotes
	line, quotes := []byte("abc"), []byte(`"`)

	// Parse the line
	args, err := Bytes(line, quotes)

	// Check for an error
	if err != nil {
		t.Error("Unexpected error", err)
	} //if

	// Check the len of the args
	if expected, actual := 1, len(args); actual != expected {
		t.Errorf("Expected length of %d but was %d", actual, expected)
	} else {
		// Check the args
		checkarg(args, 0, "abc")
	} //if

	// Set the line
	line = []byte("a b c")

	// Parse the line
	args, err = Bytes(line, quotes)

	// Check for an error
	if err != nil {
		t.Error("Unexpected error", err)
	} //if

	// Check the len of the args
	if expected, actual := 3, len(args); actual != expected {
		t.Errorf("Expected length of %d but was %d", expected, actual)
	} else {
		// Check the args
		checkarg(args, 0, "a")
		checkarg(args, 1, "b")
		checkarg(args, 2, "c")
	} //if

	// Set the line
	line = []byte(`a "b c"`)

	// Parse the line
	args, err = Bytes(line, quotes)

	// Check for an error
	if err != nil {
		t.Error("Unexpected error", err)
	} //if

	// Check the len of the args
	if expected, actual := 2, len(args); actual != expected {
		t.Errorf("Expected length of %d but was %d", expected, actual)
	} else {
		// Check the args
		checkarg(args, 0, "a")
		checkarg(args, 1, "b c")
	} //if

	// Set the line
	line = []byte(`a "b c`)

	// Parse the line
	_, err = Bytes(line, quotes)

	// Check for an error
	if err != ErrNoQuoteTerm {
		t.Error("Expected error for missing quote")
	} //if
} //func

func BenchmarkByteBuffNoCap(b *testing.B) {
	var buff bytebuff
	var c byte = 'a'

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buff.add(c)
	} //for
} //func

func BenchmarkByteBuffCap(b *testing.B) {
	buff := make(bytebuff, 0, b.N)
	var c byte = 'a'

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		buff.add(c)
	} //for
} //func
