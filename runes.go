package cmdparse

var (
	runeSpace = []rune(" \t\n\r")
)

type runebuff []rune

func (rb *runebuff) add(c rune) {
	*rb = append(*rb, c)
} //func

func (rb *runebuff) reset() {
	*rb = (*rb)[:0]
} //func

func runeIn(c rune, s []rune) bool {
	for _, sc := range s {
		if sc == c {
			return true
		} //if
	} //for
	return false
} //func

// Runes parses a line from runes into a slice of strings. If a character from
// quotes is encountered then it will add add all into a string until the
// matching character is found. If no matching terminating quote is found an
// error is returned.
func Runes(line, quotes []rune) (s []string, err error) {
	var (
		buff      = make(runebuff, 0, len(line))
		lastquote rune
	)

	checkbuff := func() {
		// Check if the buffer has any contents
		if len(buff) > 0 {
			// Add the string
			s = append(s, string(buff))

			// Reset the buffer
			buff.reset()
		} //if
	} //func

	// Loop through the bytes in the line
	for _, b := range line {
		// Determine what to do with the byte
		switch {
		case lastquote != 0 && b != lastquote:
			buff.add(b)
		case lastquote != 0 && b == lastquote:
			lastquote = 0
		case runeIn(b, quotes):
			checkbuff()
			lastquote = b
		case runeIn(b, runeSpace):
			checkbuff()
			continue
		default:
			buff.add(b)
		} //switch
	} //for

	// Final check of the buffer
	checkbuff()

	// Check for a missing termination quote
	if lastquote != 0 {
		err = ErrNoQuoteTerm
	} //if

	return
} //func
